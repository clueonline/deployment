echo "Setting password for api user"
docker exec -it $1 runuser -l postgres -c 'createuser -d -P api'
docker exec -it $1 runuser -l postgres -c 'createdb -O api api'

echo -e "\nSetting password for keycloak user"
docker exec -it $1 runuser -l postgres -c 'createuser -d -P keycloak'
docker exec -it $1 runuser -l postgres -c 'createdb -O keycloak keycloak'