domains=(demo.jfitzpatrick.me api.jfitzpatrick.me auth.jfitzpatrick.me pgadmin.jfitzpatrick.me)
data_path="./data/certbot"


#for domain in ${domains[@]}; do
#  echo "### Creating dummy certificate for $domain ..."
#  path="/etc/letsencrypt/live/$domain"
#  echo "$data_path/conf/live/$domain"
#
#done

domain_args=""
for domain in "${domains[@]}"; do
  domain_args="$domain_args -d $domain"
done

echo $domain_args